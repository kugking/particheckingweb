<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <title>Participant Checking</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-responsive.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-responsive.css">
  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" >
  <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" >
  <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" >
</head>

<body class="container">

  <?php include("menubar_view.php"); ?>
  <div class="progress" style="height:2px;margin-bottom:0px;">
    <div class="bar" style="width:100%;padding:0px;margin:0px;"></div>
  </div>
<div class="well">
 <?php echo form_open('registration/regist'); ?>

    <fieldset >

      <legend>Register</legend>

        <label for='text' >username</label>
        <input type='text' name='username_registration'maxlength="20" value="<?php echo set_value('username_registration'); ?>" />
        <?php echo form_error("username_registration","<font color='error'>","</font>"); ?>
          
      <br>

      <label for='' >password</label>
      <input type='password' name='password_registration'maxlength="20" value="<?php echo set_value('password_registration'); ?>" />
      <?php echo form_error("password_registration","<font color='error'>","</font>"); ?>
       <br>
      <label for='upassword' >confirm-password</label>
      <input type='password' name='confirm_password_registration'maxlength="50" value="<?php echo set_value('confirm_password_registration'); ?>" />
      <?php echo form_error("confirm_password_registration","<font color='error'>","</font>"); ?>

       <hr>

      <label for='text' >Firstname</label>
      <input type='text' name='firstname_registration'maxlength="50" value="<?php echo set_value('username_registration'); ?>"/>
      <?php echo form_error("firstname_registration","<font color='error'>","</font>"); ?>
      <br>
      <label for='text' >Lastname</label>
      <input type='text' name='lastname_registration' maxlength="50" value="<?php echo set_value('lastname_registration'); ?>" />
      <?php echo form_error("lastname_registration","<font color='error'>","</font>"); ?>
      <br>
      <label for='job' >Your job</label>
      <input type='text' name='job_registration'maxlength="50" value="<?php echo set_value('job_registration'); ?>"/>
      <?php echo form_error("job_registration","<font color='error'>","</font>"); ?>
      <br>
      <label for='email' >E-mail</label>
      <input type='text' name='email_registration'maxlength="50" value="<?php echo set_value('email_registration'); ?>" />
      <?php echo form_error("email_registration","<font color='error'>","</font>"); ?>
      <br>
      <label for='text' >Address</label>
      <input type='text' name='address_registration'maxlength="50" value="<?php echo set_value('address_registration'); ?>" />
      <?php echo form_error("address_registration","<font color='error'>","</font>"); ?>
      <br>
      <label for='text' >Gender</label>
      <input type='radio' name='gender_registration' value="F"<?php echo set_radio('gender_registration','1','TRUE'); ?>  />F
      <input type='radio' name='gender_registration' value="M"<?php echo set_radio('gender_registration','2'); ?> />M
      <?php echo form_error("gender_registration","<font color='error'>","</font>"); ?>
      <br>
      <br>
      <br>
      <input type='submit' class="btn btn-info" name='Submit' value='Submit' />
 
    </fieldset>
    <?php echo form_close(); ?>

    </div>

</body>
</html>



 