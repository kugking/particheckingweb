<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <title>Participant Checking</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-responsive.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap-responsive.css">
  <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet" type="text/css" >
  <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" >
  <link href="<?php echo base_url();?>css/style.css" rel="stylesheet" type="text/css" >
</head>

<body class="container">
    <?php include("menubar_view.php"); ?>
  <div class="progress" style="height:2px;margin-bottom:0px;">
    <div class="bar" style="width:100%;padding:0px;margin:0px;"></div>
  </div>
  <div class="well">
    <fieldset>
      <legend>Login</legend>
      
      <?php echo form_open('verifylogin'); ?>

      <label for="username">Username:</label>
      <input type="text" size="20" id="username_login" name="username_login"/>
      <?php echo form_error("username_login","<font color='error'>","</font>"); ?>
    <br/>

    <label for="password">Password:</label>
    <input type="password" size="20" id="passowrd_login" name="password_login"/>
    <?php echo form_error("password_login","<font color='error'>","</font>"); ?>
  <br/>

  <input class="btn btn-info" type="submit" value="Login"/>

  <?php echo form_close(); ?>

  </fieldset>

  </div>


</body>
</html>

