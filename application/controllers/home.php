<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function index()
  {
  	$this->load->helper('form');
    
  	$this->load->view('home_view');
    
  }
  function openlogin(){
  	$data=array(
  			'filename'=>'login'

  		);
  	$this->load->view('home_view',$data['filename']);
  }
  


}

?>