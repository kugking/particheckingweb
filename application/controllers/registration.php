<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registration extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('registor','',TRUE);
	}

	function index()
	{
		$this->load->helper('form');
		
  		$this->load->view("registration_view");
		
	}
	function regist(){
		//This method will have the credentials validation
		$this->load->library('form_validation');
		
		//$this->load->helper(array('form', 'url'));
		
		$frm=$this->form_validation;
		
		$config= array(
					
				array('field'=>'username_registration',
						'label'=>'Username',
						'rules'=>'required|callback_check_username'),
				array('field'=>'password_registration',
						'label'=>'Password',
						'rules'=>'required|min_length[6]|max_length[18]'),
				array('field'=>'confirm_password_registration',
						'label'=>'Confirm_Password',
						'rules'=>'required|matches[password_registration]'),
				array('field'=>'firstname_registration',
						'label'=>'Fistname',
						'rules'=>'required'),
				array('field'=>'lastname_registration',
						'label'=>'Lastname',
						'rules'=>'required'),
				array('field'=>'job_registration',
						'label'=>'Job',
						'rules'=>''),
				array('field'=>'address_registration',
						'label'=>'Address',
						'rules'=>''),
				array('field'=>'email_registration',
						'label'=>'Email',
						'rules'=>'required|valid_email|callback_check_email'),
				array('field'=>'gender_registration',
						'label'=>'Gender',
						'rules'=>'required|min_length[1]|max_length[1]'),
					
		);
		
		$frm->set_rules($config);
		$frm->set_message("required","**");
		
		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page
			$this->load->view('registration_view');
		}
		else
		{
			$this->add_new_user();
		
		}
	}
	function check_username($username_registration){

  	//query the database
		$result = $this->registor->registusername($username_registration);
	
		if($result)
		{
			$this->form_validation->set_message("check_username","The Username is registed.");
			return FALSE;
		}
		else{

			return TRUE;
		}

	}

	function check_email($email_registration){

  	//query the database
		$result = $this->registor->registemail($email_registration);

		if($result)
		{
			$this->form_validation->set_message("check_email","The E-mail is registed.");
			return FALSE;
		}
		else{

			return TRUE;
		}

	}

	function add_new_user(){

			$arrRegistrationData = array(
				'username'=>$this->input->post('username_registration'),
				'password'=>$this->input->post('password_registration'),
				'firstname'=>$this->input->post('firstname_registration'),
				'lastname'=>$this->input->post('lastname_registration'),
				'job'=>$this->input->post('job_registration'),
				'email'=>$this->input->post('email_registration'),
				'address'=>$this->input->post('address_registration'),
				'gender'=>$this->input->post('gender_registration'),

			);
				$result=$this->registor->save_registration_toDB($arrRegistrationData);
				if($result)
				{
					//print_r($result);
					echo "Please Login";
					$this->load->view('login_view');
				}
				else{
					//print_r($result);
					echo "Faile ,Please try again.";
					$this->load->view('login_view');
				}



	}
}
?>